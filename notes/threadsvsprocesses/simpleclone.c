#define _GNU_SOURCE
#include <sched.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/syscall.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define STACK_SIZE 4096

int programdata[500000000];

int set_program_data(void *arg)
{
  pid_t my_id = syscall(SYS_gettid);
  printf("Thread id: %d\n", my_id);
  for (size_t i = 0; i < 500000000; i++) {
    programdata[i] = my_id;
  }
  sleep(10);
  printf("PID read from data %d\n", programdata[0]);

  return 0;
}

int main (int argc, char **argv)
{
  pid_t thread_id = 0;
  char *child_stack;

  child_stack = malloc(STACK_SIZE);
  if (!child_stack) return 1;

  thread_id = clone(set_program_data, child_stack + STACK_SIZE, SIGCHLD|CLONE_VM,NULL,NULL,NULL);

  sleep(1);

  set_program_data(NULL);

  waitpid(thread_id, NULL, 0);

  free(child_stack);

  return 0;
}
