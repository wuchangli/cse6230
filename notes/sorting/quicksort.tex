\documentclass[12pt]{article}
\usepackage{amsmath,amssymb}
\usepackage[margin=1in]{geometry}
\usepackage[ruled,lined,linesnumbered,noend]{algorithm2e}
\title{Quicksort Sort: Parallel Analysis}
\author{Toby Isaac}
\date{CSE 6230, Fall 2017}

\newcommand{\lgtwo}{\log_2}
\newcommand{\half}{{\textstyle \frac{1}{2}}}

\begin{document}
\maketitle

\section{Pseudocode}

Note that this is for quicksort as written when project 2 was assigned.
Improvements are possible.

\begin{algorithm}
  \caption{%
    \texttt{sortQuicksort}($r$, $P$, $n$, $A$)%
  }%
  \KwData{%
    $r$ is the process's rank; \\
    $P$ is the number of processes (power of 2 in this pseudocode); \\
    $n$ is the number of local keys (may differ between processes); \\
    $A$ is the array of keys.%
  }%

  \tcc{local sort with no special condition, $O(n \lgtwo n)$}
  \texttt{sortLocal}($n$, $A$, $d$)

  \If{$P > 1$}{%
    $p \leftarrow \texttt{choosePivot}$($r$, $P$, $n$, $A$)

    \tcc{Binary search for where $p$ fits between keys in $A$ ($O \lgtwo n$)}
    $n_{\text{lo}}, n_{\text{hi}} \leftarrow \texttt{split}$($n$, $A$, $p$)

    $n_{\text{new}}, A_{\text{new}} \leftarrow \texttt{swapQuicksort}$($r$, $P$, $n_{\text{lo}}$, $n_{\text{hi}}$, $A$, $d$)

    \texttt{sortQuicksort}($r \mod P / 2$, $P / 2$, $n_{\text{new}}$, $A_{\text{new}}$)

    \tcc{$A_{\text{new}}$ is sorted, but may have wrong size}
    \tcc{repartition to restore original keys per process}
    \texttt{repartition}($r$, $P$, $n_{\text{new}}$, $A_{\text{new}}$, $n$, $A_{\text{new}}$)
  }%
\end{algorithm}

\begin{algorithm}
  \caption{%
    \texttt{choosePivot}($r$, $P$, $n$, $A$)%
  }%
  \KwData{%
    $r$ is the process's rank; \\
    $P$ is the number of processes (power of 2); \\
    $n$ is the number of local keys (same on all processes); \\
    $A$ is the array of keys.%
  }%
  \lIf{$r = 0$}{%
    $p \leftarrow A[n/2]$
  }%
  \tcc{Broadcast is $O(\lgtwo P)$}
  $p \leftarrow \texttt{Bcast}$($p$, 0, $P$)

  \KwRet{$p$}
\end{algorithm}

\begin{algorithm}
  \caption{%
    \texttt{swapQuicksort}($r$, $P$, $n$, $A$, $d$)%
  }%
  \KwData{%
    $r$ is the process's rank; \\
    $P$ is the number of processes (power of 2); \\
    $n_{\text{lo}}$ is the number of local keys less than or equal to the pivot; \\
    $n_{\text{hi}}$ is the number of local keys greater than to the pivot; \\
    $A$ is the array of keys.%
  }%
  \tcc{Communicating partner in the other half}
  $q \leftarrow r + P/2 \mod P$

  \eIf{$r < q$}{%
    $n_{\text{send}}, A_{\text{send}} \leftarrow n_{\text{hi}}, A_{\text{hi}}$

    $n_{\text{keep}}, A_{\text{keep}} \leftarrow n_{\text{lo}}, A_{\text{lo}}$
  }{%
    $n_{\text{send}}, A_{\text{send}} \leftarrow n_{\text{lo}}, A_{\text{lo}}$

    $n_{\text{keep}}, A_{\text{keep}} \leftarrow n_{\text{hi}}, A_{\text{hi}}$
  }%
  $R \leftarrow \texttt{Isend}$($A_{\text{send}}$, $n_{\text{send}}$, $q$)

  \tcc{New size unknown, can either get upper bound on possible buffer}
  \tcc{or probe for message size, allocate buffer, and receive}

  $A_{\text{new}}, n_{\text{new}} \leftarrow \texttt{Probe/Recv}$($q$)

  $\texttt{Wait}$($R$)

  \KwRet{$n_{\text{keep}} + n_{\text{recv}}$, $[A_{\text{keep}}, A_{\text{recv}}]$}
\end{algorithm}

\section{Runtime analysis}

We want an expression for $T_q(n,p)$, the runtime of parallel quicksort in
terms of $n$, the number of local keys, and $P$, the number of processes.

First the base case:
%
\[
  T_q(n,1) = T_l(n) := T_{\texttt{localSort}}(n) \in O(n\lgtwo n).
\]

Then the recursive case:
%
\[
  \begin{aligned}
    T_q(n,P) = &T_l(n)\ + \\
    &(T_{cp}(n,P) := T_{\texttt{choosePivot}}(n,P))\ + \\
    &(T_{spl}(n) := T_{\texttt{split}}(n))\ + \\
    &(T_{sq}(n,n_{\texttt{new}},P) := T_{\texttt{swapQuicksort}}(n,n_{\texttt{new}},P))\ + \\
    &T_q(n_{\texttt{new}},P/2)\ + \\
    &(T_{rp}(n,n_{\texttt{new}},P) :=
    T_{\texttt{repartition}}(n,n_{\texttt{new}},P))
  \end{aligned}
\]

We are going to make some simplifying assumptions:
%
\begin{enumerate}
  \item
    $T_l(n) = C_l n \lgtwo n$
  \item
    Since we assume in the code that $T_{cp}(n,P)\in O(\lgtwo P)$, we will assume that it
    is independent of $n$, and $T_{cp}(n,P) = C_{cp} \lgtwo P$.
  \item
    $T_{spl}(n) = C_{spl} \lgtwo N$.
  \item
    $T_{sq}(n,n_{\text{new}},P) = C^0_{sq} +
    C^1_{sq}\max\{n,n_{\text{new}}\}$, where we include the constant term
    because we think that latency may be important.
  \item
    We will ignore $T_{rp}$ for the current analysis and come back to it
    later.
  \item
    We will assume that, based on the quality of the pivot, the largest value
    of $n_{\text{new}}$ over all processes grows by some factor $1 \leq \theta
    < 2$ at each iteration, so we will use $n_{\text{new}} = \theta n$.  For
    example, I think that if the starting keys are uniformly random, and the
    pivot is chosen uniformly randomly over all keys, we would have $\theta =
    3/2$.
\end{enumerate}

Then
%
\[
  T_q(n,P) = C_l n \lgtwo n + C_{cp} \lgtwo P + C_{spl} \lgtwo n + C^0_{sq} + C^1_{sq} \theta n
  + T_q(\theta n, P/2).
\]

Expanding this recurrence:
%
\[
  \begin{aligned}
    T_q(n,P) =\ &C_l \phantom{\theta^2}n \lgtwo \phantom{(\theta}\,n\phantom{)} + C_{cp} \lgtwo P\phantom{/2} + C_{spl} \lgtwo
    \phantom{(\theta}\,n\phantom{)} +  C^0_{sq} + C^1_{sq} \phantom{\theta^2}n\  + \\
    &C_l \theta\phantom{^2} n \lgtwo (\theta\phantom{^2}n) + C_{cp} \lgtwo P/2 + C_{spl} \lgtwo (\theta\phantom{^2} n) +  C^0_{sq} + C^1_{sq} \theta\phantom{^2} n\ + \\
    &C_l \theta^2 n \lgtwo (\theta^2 n) + C_{cp} \lgtwo P/4 + C_{spl} \lgtwo (\theta^2 n) +  C^0_{sq} + C^1_{sq} \theta^2 n\ + \\
    &\vdots\\
    &C_l \theta^{\lgtwo P - 1} n \lgtwo (\theta^{\lgtwo P - 1} n) + C_{cp}
    \lgtwo 2
    + C_{spl} \lgtwo (\theta^{\lgtwo P - 1} n) + C^0_{sq} + C^1_{sq} \theta^{\lgtwo P - 1} n\ + \\
    &C_l \theta^{\lgtwo P} n \lgtwo (\theta^{\lgtwo P} n).
  \end{aligned}
\]

Let us introduce to terms in $\theta$ and $\lgtwo P$,
\[
  \begin{aligned}
    S_1(\theta,\lgtwo P) &:= \sum_{i = 0}^{\lgtwo P} \theta^i =
    \frac{\theta^{\lgtwo P + 1} - 1}{\theta - 1}, \\
    S_2(\theta,\lgtwo P) &:= \lgtwo \theta \sum_{i = 0}^{\lgtwo P} i \theta^i.
  \end{aligned}
\]

Then we can write the expansion of $T_q(n,P)$ as
%
\[
  \begin{aligned}
    T_q(n,P) =\ &C_l (S_1(\theta,P) n\lgtwo n + S_2(\theta,P) n) + C_{cp}
    \half(\lgtwo^2 P + \lgtwo P)\  + \\
    &C_{spl} (\lgtwo n \lgtwo P + \lgtwo \theta \half (\lgtwo P - 1)\lgtwo P)\ + \\
    &C^0_{sq} \lgtwo P + C^1_{sq} S_1(\theta,P/2) n.
  \end{aligned}
\]

In the case of perfect pivot selection, $\theta = 1$, this becomes a lower
bound: (Note that $S_1(1,P) = \lgtwo P + 1$ and $S_2(1,P) = 0$.)
%
\[
  \begin{aligned}
    T_q(n,P) \geq\ &C_l (\lgtwo P + 1)n\lgtwo n + C_{cp}
    \half(\lgtwo^2 P + \lgtwo P)\  + \\
    &C_{spl} (\lgtwo n \lgtwo P)\ + \\
    &C^0_{sq} \lgtwo P + C^1_{sq} n \lgtwo P.
  \end{aligned}
\]

In the worst case of always choosing an endpoint pivot, $\theta = 2$, this
becomes an upper bound: (Note that $S_1(2,P) = 2P - 1$ and $S_2(2,P) = 2 P (\lgtwo P - 1) + 1$.)
%
\[
  \begin{aligned}
    T_q(n,P) \leq\ &C_l ((2P - 1) n\lgtwo n + (2P(\lgtwo P - 1) + 1) n) + C_{cp}
    \half(\lgtwo^2 P + \lgtwo P)\  + \\
    &C_{spl} (\lgtwo n \lgtwo P + \half (\lgtwo P - 1)\lgtwo P)\ + \\
    &C^0_{sq} \lgtwo P + C^1_{sq} (P - 1) n.
  \end{aligned}
\]

\end{document}
