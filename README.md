# CSE 6320 (Fall 2017, 3 Credit Hours) | High Performance Parallel Computing: Tools and Applications

## Contact

|          | **Instructor**                                      | **Teaching Assistant**                            | **Teaching Assistant**                            |
| ---      | :------------------                                 | :------------------                               | :------------------                               |
| *name*   | Toby Isaac                                          | Ankit Srivastava                                  | Saminda Wijeratne                                 |
| *email*  | [tisaac@cc.gatech.edu](mailto:tisaac@cc.gatech.edu) | [asrivast@gatech.edu](mailto:asrivast@gatech.edu) | [samindaw@gatech.edu](mailto:samindaw@gatech.edu) |
| *phone*  | [(404)385-5970](tel:+14043855970)                   |                                                   |                                                   |
| *office* | [KACB 1302](https://goo.gl/38x6QB)                  | [KACB CSE Common Area](https://goo.gl/38x6QB)     | [KACB CSE Common Area](https://goo.gl/38x6QB)     |
| *hours*  | Monday 9--10 a.m. & Wednesday 2-3 p.m.              | Tuesday & Friday 11--12 p.m.                      | Monday 2--3 p.m. & Thursday 11--12 p.m.           |

## Website

Lecture notes, discussion, updates, and most assignments will be managed by the repository for this course:
[https://bitbucket.org/cse6230fa17/cse6230](https://bitbucket.org/cse6230fa17/cse6230).

## Questions & Discussion

A Piazza discussion page has been created for this course:
[https://piazza.com/gatech/fall2017/cse6230/home](https://piazza.com/gatech/fall2017/cse6230/home).

Your fellow students have a lot of expertise, and could benefit from yours.  Please take advantage of that with discussion on Piazza.

## Lectures

Tuesdays & Thursdays, 9:30--10:45 a.m., [Van Leer C240](https://www.google.com/maps/place/Van+Leer+Building/@33.7759997,-84.3993218)

## Description

So you've written an algorithm and coded it up.  Is your code any good?  Can it
be better?  High Performance Computing (HPC) is about answering these
questions.  Answering these questions requires *performance analysis* of how
your algorithm matches up to the machine it is running on.  This course teaches
practical performance analysis to determine when and where code optimization
can help.

This course also teaches *how* to make your code better.  These days, making
your code better almost always means taking fullest advantage of the
*concurrency* in modern architectures.  This course is full of hands-on
experience with the software tools that turn efficient parallel algorithms into
efficient parallel programs.

## Objectives

I want you to be able to:

* *Design* high-performance code.  Good design requires good algorithms for
  the problem at hand -- the subject of other courses like [CSE 6220: Intro to
  HPC](http://cse6220.gatech.edu/sp17-oms/) -- but also good understanding of
  the computer system being used in the form of *performance models*.  This
  course will discuss practical performance modeling of current architectures,
  including multi-core, coprocessor (e.g. GPUs) and many-core designs.

* *Implement* high-performance code, drawing on a wide range software
  engineering tools, including libraries, language extensions, intrinsics, and
  compiler directives.  This course will have coding assignments that will
  familiarize you with what these tools can -- and can't -- do.

* *Analyze* high-performance code. This may require more sophisticated
  measurement than just the runtime on a given problem.  For performance to be
  reproducible and transferable, we must understand *why* it is or isn't fast.
  This course will present tools for measuring the behavior of different
  components of a computer system to identify bottlenecks and confirm whether
  the *implementation* matches the *design*.

## Prerequisite Courses and Skills

[CS 3120] is listed as a prerequisite on OSCAR.
Many of the concepts from that course listing (multi-threading, scheduling,
synchronization, and communication) will be relevant to this course, but
background references will be provided throughout.

This course can be thought of as the connection between [CSE 6220] and [CS
6290].  Neither is a prerequisite, but we will connect to some of the material
from each, and those who find themselves interested more in the algorithmic or
in the hardware aspects of this course can peruse the materials for those
courses for more depth.

[CS 3120]: https://oscar.gatech.edu/pls/bprod/bwckctlg.p_disp_course_detail?cat_term_in=201708&subj_code_in=CS&crse_numb_in=3210
[CSE 6220]: http://cse6220.gatech.edu/sp17-oms/
[CS 6290]: http://www.omscs.gatech.edu/cs-6290-high-performance-computer-architecture/

Code examples from this course will be written in C or C++; assignments may be
submitted C, C++, or Fortran.  Most HPC computing systems run POSIX-y operating
systems, so basic comfort with command line tools is expected, and a
willingness to learn new tools will make life a lot easier.

## Tentative Schedule

| Week     | Dates                          | Topics                                  | Assignments                                            | Reading                   |
| ----:    | -----                          | ------                                  | -----------                                            | -------                   |
| 1        | Aug. 22 & 24                   | - [Introduction](notes/introduction)    | - [01](exercises/01-Introduction-STREAM)               | [Introduction][readintro] |
|          |                                | - [STREAM](notes/STREAM)                | - [02](exercises/02-STREAM-OpenMP)                     | [STREAM][readstream]      |
|          |                                | - [OpenMP (introduction)](notes/openMP) |                                                        | [OpenMP][readopenmp]      |
| 2        | Aug. 29 & 31                   | - [git](notes/git)                      | - [03](exercises/03-Brownian-Motion)                   | [git][readgit]            |
|          |                                | - [OpenMP (continued)](notes/openMP)    | - [04](exercises/04-Repulsive-Particles)               |                           |
| 3        | Sep. 5 & 7                     | - the deepthought cluster               | - [**Final: Checkpoint 0 due Fri. Sept. 8**][fp0]      |                           |
|          |                                | - [Pseudo-random number generators][rn] | - [Project 1 assigned][p1]                             | [PRNGs][readprngs]        |
|          |                                | - Thread affinity & NUMA                |                                                        | [caches][readcaches]      |
|          |                                | - [Caches, coherence & false sharing][c]|                                                        |                           |
|          |                                | - App: molecular simulation             |                                                        |                           |
| 4        | **No class**                   |                                         | - [Work on project 1][p1]                              |                           |
| 5        | Sep. 19 & 21                   | - [App: stencils][stencils]             | - [**Project 1 due Mon. Sept. 18**][p1]                | [stencils][readstencils]  |
| 6        | Sep. 26 & 28                   | - [Threads vs. processes][tvp]          | - [**Final: Checkpoint 1 due Fri. Sept. 29**][fp1]     | [TvP][readtvp]            |
|          |                                | - [MPI (point-to-point)][p2p]           |                                                        | [MPI][readmpi]            |
|          |                                | - [Hybrid parallelism][hybrid]          |                                                        |                           |
| 7        | Oct. 3 & 5                     | - MPI (collectives)                     | - [05](exercises/05-MPI-Benchmarks)                    |                           |
|          |                                | - MPI (remote memory access)            |                                                        |                           |
| 8        | Oct. 12                        | - MPI (shared memory)                   | Project 2 assigned                                     |                           |
|          | **Fall Recess**                |                                         |                                                        |                           |
| 9        | Oct. 17 & 19                   | - GPUs                                  | Work on project 2                                      |                           |
|          |                                | - CUDA                                  |                                                        |                           |
| 10       | Oct. 24 & 26                   | - CUDA                                  | **Project 2 due Sat. Oct. 28**                         |                           |
|          |                                | - sparse matrices and graphs            |                                                        |                           |
| 11       | Oct. 31 & Nov. 2               | - CUDA                                  |                                                        |                           |
|          |                                | - SIMD                                  |                                                        |                           |
| 12       | Nov. 7 & 9                     | - graph ordering / partitioning         | - [**Final: Checkpoints 2 & 3 due Fri. Nov. 10**][fp3] |                           |
|          |                                |                                         | Project 3 assigned                                     |                           |
| 13       | Nov. 14 & 16                   | - performance analysis                  | Work on project 3                                      |                           |
| 14       | Nov. 21                        | - PGAS?                                 | **Project 3 due Mon. Nov. 20**                         |                           |
|          | **Thanksgiving**               | - Interconnects?                        |                                                        |                           |
|          |                                | - Other Apps?                           |                                                        |                           |
| 15       | Nov. 28 & 30                   | - Cilk plus                             | Work on final project                                  |                           |
| 16       | Dec. 5 (**Final Instruction**) | - Libraries (BLAS, LAPACK, MKL)         | None                                                   |                           |
|          | Dec. 14                        |                                         | - [**Final project due Thur. Dec. 14**][fp4]           |                           |
|          |                                |                                         |                                                        |                           |


[stencils]: notes/stencil
[rn]: notes/PRNGs
[c]: notes/caches
[tvp]: notes/threadsvsprocesses
[p2p]: https://bitbucket.org/cse6230fa17/cse6230/src/master/notes/mpi/#markdown-header-point-to-point-communication
[hybrid]: https://bitbucket.org/cse6230fa17/cse6230/src/master/notes/mpi/#markdown-header-hybrid-parallelism
[readintro]: https://bitbucket.org/cse6230fa17/cse6230/src/master/notes/introduction/#markdown-header-additional-reading
[readstream]: https://bitbucket.org/cse6230fa17/cse6230/src/master/notes/STREAM/#markdown-header-additional-reading
[readopenmp]: https://bitbucket.org/cse6230fa17/cse6230/src/master/notes/openMP/#markdown-header-additional-reading
[readgit]: https://bitbucket.org/cse6230fa17/cse6230/src/master/notes/git/#markdown-header-additional-reading
[readprngs]: https://bitbucket.org/cse6230fa17/cse6230/src/master/notes/PRNGs/#markdown-header-additional-reading
[readcaches]: https://bitbucket.org/cse6230fa17/cse6230/src/master/notes/caches/#markdown-header-additional-reading
[readstencils]: https://bitbucket.org/cse6230fa17/cse6230/src/master/notes/stencil/#markdown-header-additional-reading
[readmpi]: https://bitbucket.org/cse6230fa17/cse6230/src/master/notes/mpi/#markdown-header-additional-reading
[readtvp]: https://bitbucket.org/cse6230fa17/cse6230/src/master/notes/threadsvsprocesses/#markdown-header-additional-reading
[fp0]: https://bitbucket.org/cse6230fa17/cse6230/src/master/finalproject/#markdown-header-checkpoint-0-choose-your-project-team
[fp1]: https://bitbucket.org/cse6230fa17/cse6230/src/master/finalproject/#markdown-header-checkpoint-1-serial-analysis-implementation
[fp2]: https://bitbucket.org/cse6230fa17/cse6230/src/master/finalproject/#markdown-header-checkpoint-2-parallel-analysis-machine-choice
[fp3]: https://bitbucket.org/cse6230fa17/cse6230/src/master/finalproject/#markdown-header-checkpoint-3-parallel-implementation
[fp4]: https://bitbucket.org/cse6230fa17/cse6230/src/master/finalproject/#markdown-header-final-parallel-optimization
[p1]: https://bitbucket.org/cse6230fa17/cse6230/src/master/projects/1-Molecular-Dynamics

## Assignments & Grading

|                       |     |
| ---                   | --- |
| Exercises (1--2/week) | 30% |
| Projects (3)          | 30% |
| Final Project         | 40% |

* **Exercises**: Exercises are mostly about practical experience.  They are
  typically assigned at the end of a lecture, due about three days later, and
  graded on a 2-point scale.  They are to be completed individually and
  submitted using [T-Square](https://t-square.gatech.edu).  They will not be
  accepted late.  The lowest two grades will be dropped.
* **Projects**:  Projects will involve a combination of implementation and
  analysis to achieve high-performance.  Projects will require you to fork code
  from us, and for us to run your code.  To make it easier for us to do so, the
  central repository will be used for code submissions instead of T-Square.
  We will discuss this in detail in class. 
  There are three projects planned; more details about the grading scale of
  each will be available when it is assigned.  They will not be accepted late.
  Students may work alone or in pairs.
* **Final Project**: The final project will involve each of the three main
  aspects of design, implementation, and analysis.  Projects may be chosen from
  a set of pre-defined research questions, but that set will take into account
  student interests as expressed as part of the exercise assigned the first day
  of class.  Students may work alone or in pairs.

For all assignments, students may collaborate through discussion, but all
coding and writing should be done by the project members.  

**Grades** will be earned according to the standard scale:

|     |          |
| --- | ---      |
| A   | 90--100% |
| B   | 80--90%  |
| C   | 70--80%  |
| D   | 60--70%  |
| F   | 0--60%   |

* **Passing** grades for Pass/Fail students are D or better.

* **Auditing** students are welcome to come to office hours to discuss
  exercises and projects; they may also submit code components of projects for
  automatic evaluation (e.g., we will put their submissions into the queue for
  benchmarking scripts).  They will not, however, receive graded feedback.

* **Unregistered** students interested in shadowing this course have expressed
  concern that they do not have access to T-Square.  All course materials and
  assignments are in this repository, and all discussion will hopefully take
  place in person or on Piazza (which I believe they can access), so they
  aren't missing a thing.

## Textbooks and Reading

* *Highly Recommended*: __Introduction to High Performance Computing for
  Computational Scientists and Engineers__, by Georg Hager and Gerhard Wellein.
  ISBN: 978-1-4398-1192-4, CRC Press, 2010.

* Optional: __Introduction to Parallel Computing__, by Grama et al.,
  Addison-Wesley, 2003. Available online via the Georgia Tech Library's
  website:
  [www](http://proquest.safaribooksonline.com.www.library.gatech.edu:2048/book/software-engineering-and-development/0201648652)

This is my first time teaching this course.  I will be looking lots of places
for good references, and I plan on sharing whatever I find.  Excellent
resources include:

* [Professor Vuduc's lectures](http://vuduc.org/cse6230/)
* [Professor Chow's lectures](https://www.cc.gatech.edu/~echow/ipcc/hpc-course/)
* Georg Hager's lecture notes: follow the instructions under "Teaching
  material" on his [book's website](https://blogs.fau.de/hager/hpc-book)

I will also occasionally assign portions of research papers that I think
are good illustrations of the topics of this course.

## Academic Integrity

Georgia Tech aims to cultivate a community based on trust, academic integrity,
and honor. Students are expected to act according to the highest ethical
standards.  For information on Georgia Tech's Academic Honor Code, please visit
[http://www.catalog.gatech.edu/policies/honor-code/](http://www.catalog.gatech.edu/policies/honor-code/)
or [http://www.catalog.gatech.edu/rules/18/](http://www.catalog.gatech.edu/rules/18/).

## Accomodations for Individuals with Disabilities

If you are a student with learning needs that require special accommodation,
contact the Office of Disability Services at [(404)894-2563](tel:+14048942563)
or
[http://disabilityservices.gatech.edu/](http://disabilityservices.gatech.edu/),
as soon as possible, to make an appointment to discuss your special needs and
to obtain an accommodations letter.  Please also e-mail me as soon as possible
in order to set up a time to discuss your learning needs.


