#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <tictoc.h>
#include <cse6230rand.h>
#if defined(_OPENMP)
#include <omp.h>
#endif

#if !defined(N)
#define N 10000
#endif

#if !defined(DIM)
#define DIM 3
#endif

static double x[DIM*N];
static double x0[DIM*N];

int main(int argc, char **argv)
{
  int         Nsteps  = 5000;
  int         Nreport = 500;
  double      dt      = 1.e-4;
  TicTocTimer timer;
  double      dfuse, avg, walltime;

  if (argc > 1) Nsteps  = atoi(argv[1]);
  if (argc > 2) Nreport = atoi(argv[2]);
  if (argc > 3) dt      = atof(argv[3]);

  dfuse = sqrt(2. * dt);

  timer = tic();
  #pragma omp parallel
  {
    cse6230rand_t crand;
    unsigned int  seed = 0;
    int           t, p;

#if defined(_OPENMP)
    seed = omp_get_thread_num();
#endif
    cse6230rand_seed(seed,&crand);

    #pragma omp for schedule(runtime)
    for (p = 0; p < DIM * N; p++) {
      double rval = cse6230rand(&crand);

      x[p] = x0[p] = rval;
    }

    for (t = 0; t < Nsteps; t++) {
      if (!(t % Nreport)) {
        #pragma omp single
        {
          avg = 0.;
        }
        #pragma omp for reduction(+:avg) schedule(runtime)
        for (p = 0; p < N; p++) {
          int    d;
          double dist, dist2 = 0.0;

          for (d = 0; d < DIM; d++) {
            double diff = x[DIM*p + d]- x0[DIM*p + d];

            dist2 += diff * diff;
          }
          dist = sqrt(dist2);
          avg += dist;
        }
        #pragma omp single nowait
        {
          avg /= N;
          printf("Average distance after %4d steps: %e\n",t,avg);
        }
      }
      #pragma omp for schedule(runtime)
      for (p = 0; p < DIM * N; p++) {
        double rval  = cse6230rand(&crand);
        double noise = rval * 2. - 1.;

        x[p] += dfuse * noise;
      }
    }
    if (!(t % Nreport)) {
      #pragma omp single
      {
        avg = 0.;
      }
      #pragma omp for reduction(+:avg) schedule(runtime)
      for (p = 0; p < N; p++) {
        int    d;
        double dist, dist2 = 0.0;

        for (d = 0; d < DIM; d++) {
          double diff = x[DIM*p + d]- x0[DIM*p + d];

          dist2 += diff * diff;
        }
        dist = sqrt(dist2);
        avg += dist;
      }
      #pragma omp single nowait
      {
        avg /= N;
        printf("Average distance after %4d steps: %e\n",t,avg);
      }
    }
  }
  walltime = toc(&timer);
  printf("Simulation walltime: %e seconds\n",walltime);

  return 0;
}

