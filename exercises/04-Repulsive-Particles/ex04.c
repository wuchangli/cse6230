#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <sys/time.h>
#if defined(_OPENMP)
#include <omp.h>
#endif
#include "forces.h"

#if !defined(N)
#define N 10000
#endif

#if !defined(DIM)
#define DIM 3
#endif

static double x[DIM*N];
static double x0[DIM*N];
static double forces[DIM*N];

/* After Hager & Wellein, Listing 1.2 */
static double get_walltime(void)
{
  struct timeval tp;
  gettimeofday(&tp, NULL);

  return (double) (tp.tv_sec + tp.tv_usec/1.e6);
}

int main(int argc, char **argv)
{
  int    m;
  int    Nsteps  = 5;
  double dt      = 1.e-4;
  double M       = 1.;
  double dfuse, avg, walltime;
  const char *method[3] = {"critical","atomic","duplicate"};
  void (*(force_funcs[3]))(int,const double *restrict,double *restrict) = {compute_forces_critical,compute_forces_atomic,compute_forces_duplicate};

  if (argc > 1) Nsteps  = atoi(argv[1]);
  if (argc > 2) dt      = atof(argv[2]);

  dfuse = sqrt(2. * dt);

  for (m = 0; m < 3; m++) {
    walltime = -get_walltime();
    #pragma omp parallel
    {
      unsigned short seed[3] = {0,0,0};
      int            t, p;

#if defined(_OPENMP)
      seed[0] = omp_get_thread_num();
#endif

      #pragma omp for
      for (p = 0; p < DIM * N; p++) {
        double rval = erand48(seed);

        x[p] = x0[p] = rval;
        forces[p] = 0.;
      }

      for (t = 0; t < Nsteps; t++) {
        (*force_funcs[m])(N,x,forces);
        #pragma omp for
        for (p = 0; p < DIM * N; p++) {
          double rval  = erand48(seed);
          double noise = rval * 2. - 1.;

          x[p] += M * forces[p] * dt + dfuse * noise;
          x[p] = remainder(x[p]-0.5,1.0) + 0.5;
          forces[p] = 0.;
        }
      }
      #pragma omp single
      {
        avg = 0.;
      }
      #pragma omp for reduction(+:avg)
      for (p = 0; p < N; p++) {
        int    d;
        double dist, dist2 = 0.0;

        for (d = 0; d < DIM; d++) {
          double diff = remainder(x[DIM*p + d]- x0[DIM*p + d],1.);

          dist2 += diff * diff;
        }
        dist = sqrt(dist2);
        avg += dist;
      }
      #pragma omp single nowait
      {
        avg /= N;
        printf("Average distance after %4d steps: %e\n",t,avg);
      }
    }
    walltime += get_walltime();
    printf("Simulation walltime (%s method): %e seconds\n",method[m],walltime);
  }

  return 0;
}

